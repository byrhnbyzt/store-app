const express = require('express');
const router = express.Router();

const mongo = require('mongodb').MongoClient;
const dburl = 'mongodb://localhost:27017/shoppity';

router.get('/catalog', (req, res) => {
	mongo.connect(dburl, (err, db) => {
		if (err) {
			console.log(err);
		}
		const products = db.collection('products');
		const query = products.find(req.params.id);
		query.toArray((err, products) => {
			if (err) {
				console.log(err);
			}
			res.render('catalog', { title: 'Catalog', items: products });
		});
	});
});

router.get('/catalog', (req, res, next) => {
	mongo.connect(dburl, (err, db) => {
		const products = db.collection('products');
		const query = products.find({});
		query.toArray((err, items) => {
			res.render('catalog', { title: 'Catalog', items: products });
		});
	});
});

module.exports = router;