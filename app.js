const express = require('express');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const expressHbs = require('express-handlebars');
const bodyParser = require('body-parser');
const logger = require('morgan');
const path = require('path');

const helpers = require('./helpers');
const errorHandlers = require('./handlers/errorHandlers');

const index = require('./routes/index');
const catalog = require('./routes/catalog');
const women = require('./routes/women');
const men = require('./routes/men');


const app = express();

// all enviroments
app.set('port', process.env.PORT || 3000);
app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', '.hbs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  res.locals.h = helpers;
  res.locals.currenthPath = req.path;
  next();
});

// app routes
app.get('/', index);
app.get('/men', men);
app.get('/women', women);
app.get('/catalog', catalog);
app.get('/catalog/:id', catalog);
// app.get('/product', product);
// app.get('/product/:id', product);


app.use(errorHandlers.notFound);
// Otherwise this was a really bad error we didn't expect! Shoot eh
if (app.get('env') === 'development') {
  app.use(errorHandlers.developmentErrors);
}
// production error handler
app.use(errorHandlers.productionErrors);

// start express server
app.listen(app.get('port'), () => {
  console.log(`💻  App is running at http://localhost:${app.get('port')} in ${app.get('env')} mode..`);
  console.log(' ⇛ Press CTRL-C to stop\n');
});