const express = require('express');
const router = express.Router();

const mongo = require('mongodb').MongoClient;
const dbUrl = 'mongodb://localhost:27017/shoppity';

router.get('/women', (req, res) => {
  mongo.connect(dbUrl, (err, db) => {
		const categories = db.collection('categories');
		const query = categories.find({ "id": "womens" });
		query.toArray((err, categories) => {
			res.render('women', { title: 'Categorie - Women', items: categories });
		});
	});
});

module.exports = router;