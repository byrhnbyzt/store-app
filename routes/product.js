const express = require('express');
const router = express.Router();

const mongo = require('mongodb').MongoClient;
const dburl = 'mongodb://localhost:27017/shoppity';

router.get('/product/:id', (req, res) => {
	mongo.connect(dburl, (err, db) => {
		const cursor = db.collection('products');
		
		cursor.findById(req.params.id, (err, items) => {
			res.render('product', { 
				title: 'Product Details',
				items: items
			});
			db.close();
		});
	});
});

module.exports = router;