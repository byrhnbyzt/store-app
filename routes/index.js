const express = require('express');
const router = express.Router();
const mongo = require('mongodb').MongoClient;
const dbUrl = 'mongodb://localhost:27017/shoppity';

router.get('/', (req, res) => {
	res.render('index', { title: 'Home' });
});

module.exports = router;