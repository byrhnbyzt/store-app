// This is a file of data and helper functions that we can expose and use in our templating function

// import environmental variables from our variables.env file
require('dotenv').config({ path: 'variables.env' });

// details about brand name
exports.brandName = `Shoppity`;

//
exports.menu = [
  { slug: '/men', title: 'Men', },
  { slug: '/women', title: 'Women', }
];
